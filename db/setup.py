#!/usr/bin/python
from db.poolmatedb import Poolmatedb

__author__ = 'knm'


class Setup:
    """Does provide a call method to initialize the couchdb and imports all data from the
    configuration directory in $HOME/.pypoolmate
    """

    def setUp(self):
        """setup will create a couchdb on the local host with name 'poolmatetest' and call
        the setup method on poolmatedb instance
        """
        self.dbname = 'poolmatetest'
        self.db = Poolmatedb(dbname=self.dbname)
        self.db.setup()

if __name__ == '__main__':
    Setup().setUp()
