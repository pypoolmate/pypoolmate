import csv
from datetime import datetime
import os
import re
from couchdb.http import ResourceNotFound
from db import importer
from model.importRecord import ImportRecord
from model.manager.recordManager import recordManager
import ast

__author__ = 'knm'

from couchdb.client import Server

class Poolmatedb:
    """This class provides access and views to the data from the couchdb.
    Convenience methods for retrieving data.

    It can provide db instance itself for testing purposes
    """

    configdirectory = '.pypoolmate'

    def __init__(self, url = 'localhost', port = '5984', dbname = 'poolmate'):
        """Initialize db with given params,

        If the database with dbnames does not exist, it will be freshly created
        """
        self.server = Server('http://' + url + ':' + port)
        try:
            self.database = self.server[dbname]
        except ResourceNotFound:
            self.server.create(dbname)
            self.database = self.server[dbname]
        self.designDocument = '_design/recordview'
        self.totaldistance_view = "_design/recordview/_view/TotalDistance"
        self.distance_view = "_design/recordview/_view/Distance"
        self.speed_view = "_design/recordview/_view/Speed"
        self.efficiency_view = "_design/recordview/_view/Efficiency"
        self.strokes_view = "_design/recordview/_view/StrokeRate"
        self.all_view = "_design/recordview/_view/All"
        self.consolidate_view = "_design/recordview/_view/Consolidator"
        self.logdate_view = "_design/recordview/_view/ByLogDate"
        self.date_view = "_design/recordview/_view/ByDate"

    def setup(self):
        """Does the initial setup with following steps

        # Add the design document containing the views for couchdb queries
        # Import all CSV files from $HOME/.pypoolmate
        """
        self.add_design_document()
        self.import_all_from_config_directory()

    def add_design_document(self):
        """Adds the design-document to the couchdb for enabling queries
        """
        try:
            oldDoc = self.database['_design/recordview']
            self.database.delete(oldDoc)
        except ResourceNotFound:
            pass
        viewcontent = open('couchdb.view.json', 'r').read()
        self.database.save(ast.literal_eval(viewcontent))
        pass

    def get_all(self):
        """Returns all records"""
        return self.database.view(self.all_view)

    def get_speed(self):
        """Returns all speed entries"""
        return self.database.view(self.speed_view)

    def get_all_distance(self):
        """Returns all distance entries"""
        return self.database.view(self.totaldistance_view)

    def get_consolidates(self):
        """Returns the consolidated view entries"""
        return self.database.view(self.consolidate_view, group_level=1)

    def get_server(self):
        """Returns the couchdb server"""
        return self.server

    def get_database(self):
        """Returns the couchdb instance"""
        return self.database

    def get_consolidated_values(self):
        """Extract the consolidated values"""
        return [doc.value for doc in self.get_consolidates()]

    def get_consolidated_log_date_times(self):
        """Extract the consolidated keys, will be datetime instances"""
        return [doc.key for doc in self.get_consolidates()]

    def get_average_data_from(self, view):
        """
        Does get all values for given view and consolidates it.
        Consolidation here means that all values from a session will be averaged, so we have a single (averaged) value
        for a single session.
        """
        data = []
        sessions = self.get_consolidates()
        for logdatetime in [doc.key for doc in sessions]:
            logdatetimekey = logdatetime[0]
            results = self.database.view(view, key=logdatetimekey)
            sessionStat = [doc.value for doc in results][0]
            avg = self.extract_avg(sessionStat)
            data.append((datetime.strptime(logdatetimekey, '%Y-%m-%dT%H:%M:%S'), avg))
        return data



    def get_plot_speed_data(self):
        return self.get_average_data_from(self.speed_view)

    def get_plot_efficiency_data(self):
        return self.get_average_data_from(self.efficiency_view)

    def get_plot_strokes_data(self):
        return self.get_average_data_from(self.strokes_view)

    def get_plot_distance_data(self):
        return self.get_average_data_from(self.distance_view)
    
    def extract_avg(self, stat):
        return stat['sum'] / stat['count']

    def import_csv_file(self, file):
        """Does import a single csv file into the couchdb
        """
        ifile = open(file, 'rb')
        reader = csv.reader(ifile, delimiter=',')
        record = list(map(ImportRecord, reader))
        imp = importer.importer(self.get_database())
        for rec in record:
            imp.insertIntoCouch(rec)

    def import_all_from_config_directory(self):
        """Does import all csv files from $HOME/.pypoolmate
        """
        configDirectory = os.environ.get('HOME') + '/' + Poolmatedb.configdirectory
        files = filter(lambda file: re.search('Log.*csv', file), os.listdir(configDirectory))
        for file in files:
            self.import_csv_file(configDirectory + '/' + file)

    def get_by_logdate(self, date):
        results = self.database.view(self.logdate_view)
        return results[date]

    def get_by_date(self,year, month, day = None):
        results = self.database.view(self.date_view)
        if day is None:
            return results[[year,month]:[year,(month+1)]]

        return results[[year,month,day]:[year,month,day]]

class dataprovider:
    """
    Helpler class to provide data access to gui
    """

    def __init__(self, host='localhost',port='5984', dbname='poolmatetest'):
        self.poolmatedb = Poolmatedb(host, port, dbname)
        pass

    def get_test_recordmanager(self):
        return recordManager([doc.value for doc in self.poolmatedb.get_all()])


