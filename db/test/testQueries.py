from datetime import datetime
from db import poolmatedb

__author__ = 'knm'

import unittest


class QueryTest(unittest.TestCase):
    """Is an integration test class, couchdb with documents must already be present.

    run the setup method. Place the CSV documents located in the rescources into $HOME/.pypoolmate
    """

    def setUp(self):
        self.dbname = 'poolmatetest'
        self.db = poolmatedb.Poolmatedb(dbname=self.dbname)

    def test_plotSpeedData(self):
        data = []
        sessions = self.db.get_consolidates()

        for logdatetime in [doc.key for doc in sessions]:
            self.assertEqual(1, len(logdatetime))
            logdatetimekey = logdatetime[0]
            results = self.db.get_database().view(self.db.speed_view,key=logdatetimekey)
            sessionStat = [doc.value for doc in results][0]
            self.assertTrue(sessionStat['count'] > 0)
            avg = self.extractAvg(sessionStat)
            data.append((datetime.strptime(logdatetimekey, '%Y-%m-%dT%H:%M:%S'), avg))
            self.assertTrue(avg > 0)

        self.assertTrue(len(data) > 0)

    def test_retrieve_by_logdate(self):
        date = '01/07/2011'
        results = self.db.get_by_logdate(date)

        for key in [doc.key for doc in results]:
            self.assertEqual(date, key)

        self.assertEquals(4, len(results))


    def extractAvg(self, stat):
        return stat['sum'] / stat['count']

        
if __name__ == '__main__':
    unittest.main()
