import csv
from db import poolmatedb, importer
from model.importRecord import ImportRecord

__author__ = 'knm'

import unittest

class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.dbname = 'unittest-pypoolmate'
        self.db = poolmatedb.Poolmatedb(dbname=self.dbname)

    def test_db_present(self):
        db = self.db.get_database()
        self.assertNotEquals(None, db)

    def test_db_docinsertion(self):
        db = self.db.get_database()
        self.assertEquals(0, len(db))
        file = 'Log4.csv2917509Done'
        ifile = open(file)
        reader = csv.reader(ifile, delimiter=',')
        record = list(map(ImportRecord, reader))
        imp = importer.importer(db)
        for rec in record:
            imp.insertIntoCouch(rec)
        firstid = record[1].id
        doc = db[firstid]
        self.assertNotEquals(None, doc)
        self.assertNotEquals(None, doc['LogDateTime'])
        self.assertEqual(21, doc['day'])
        self.assertEqual(07, doc['month'])
        self.assertEqual(2011, doc['year'])
        ifile.close()
        self.assertTrue(len(db) > 0)


    def tearDown(self):
        self.db.get_server().delete(self.dbname)


if __name__ == '__main__':
    unittest.main()
