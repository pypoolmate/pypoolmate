from db.poolmatedb import Poolmatedb

__author__ = 'knm'


import unittest

class SetupTest(unittest.TestCase):

    def setUp(self):
        self.dbname = 'poolmate-creation-test'
        self.db = Poolmatedb(dbname=self.dbname)

    def testCreateDesignDocument(self):
        self.db.add_design_document()
        database = self.db.get_database()
        recordview = database['_design/recordview']
        self.assertNotEqual(None, recordview)
        pass

    def testInitialImport(self):
        self.db.import_all_from_config_directory()
        database = self.db.get_database()
        # we should have one design-document and at least one other document
        self.assertTrue(len(database) > 1)


    def tearDown(self):
        self.db.get_server().delete(self.dbname)
        pass


