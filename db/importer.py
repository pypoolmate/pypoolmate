import couchdb


class importer:
    """Import class, receives a couchdb instance on creation

    """
    def __init__(self, database):
        self.database = database


#
    def insertIntoCouch(self, record):
        id = record.id
        try:
            self.database[id] = record.jsonize()
        except ValueError:
            print "skipping import row (header?)"
        except couchdb.http.ResourceConflict:
            print "record already exists, not overwriting"
        else:
            print "saved " + id

