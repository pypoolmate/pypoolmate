from operator import itemgetter
from db.poolmatedb import dataprovider
from model.swimRecord import swimrecord

__author__ = 'knm'

class Controller:

    def __init__(self):
        self.dataprovider = dataprovider()
        self.recordmanager = self.dataprovider.get_test_recordmanager()

    def get_all_records(self):
        records = sorted(self.recordmanager.get_all_records(), key=itemgetter('LogDate'))
        return [swimrecord(doc) for doc in records]

    def get_lowest(self, field):
        return self.recordmanager.get_lowest(field)

    def get_highest(self, field):
        return self.recordmanager.get_highest(field)

    def get_average(self, field):
        return self.recordmanager.get_average(field)

    def get_sum(self, field):
        return self.recordmanager.get_sum(field)

    def get_first(self, field):
        return self.recordmanager.get_first(field)

    def get_consolidated_values(self):
        return self.dataprovider.poolmatedb.get_consolidated_values()

    def get_column_types(self):
        return swimrecord().get_column_types()

    def get_record_by_date(self, year, month, day = None):
        return [swimrecord(doc.value) for doc in self.dataprovider.poolmatedb.get_by_date(year, month, day)]

    def get_plot_strokes_data(self):
        return self.dataprovider.poolmatedb.get_plot_strokes_data()

    def get_plot_speed_data(self):
        return self.dataprovider.poolmatedb.get_plot_speed_data()

    def get_plot_distance_data(self):
        return self.dataprovider.poolmatedb.get_plot_distance_data()

    def get_plot_efficiency_data(self):
        return self.dataprovider.poolmatedb.get_plot_efficiency_data()

    def update_record(self, swimrecord):
        self.dataprovider.poolmatedb.database[swimrecord.id] = swimrecord.record
  