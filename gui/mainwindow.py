#!/usr/bin/env python

import pygtk
import datetime
from gtktree.model import RowObjectListStore
from gtktree.util import TreeViewUtils
from controller.controller import Controller
import matplotlib
if matplotlib.__version__ < '1.1.0':
    raise ImportError('error: must use matplotlib version 1.1.0 or higher, tested only with 1.1.0')
matplotlib.use('GtkAgg')
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as Canvas
from plot.plot import Plotter

pygtk.require('2.0')
import gtk



class PyPoolmate:

    def add_column(self, treeview, columnName, index):
        cell = gtk.CellRendererText()
        cell.connect_after('edited' , self.editedCell)
        TreeViewUtils.enable_renderer_editing(cell, self.list_store, columnName)
        col = gtk.TreeViewColumn(columnName, cell, text=index)
        col.set_sort_column_id(index)
        if columnName == '_id':
            col.set_visible(False)
        treeview.append_column(col)

    def add_columns(self, treeview):
        for index, column in enumerate(self.columns):
            self.add_column(treeview,column, index)

    def build_bottom_labels(self, treeview):
        treeview.set_model(self.list_store)
        lowSpeed = self.controller.get_lowest("Speed")
        avgSpeed = self.controller.get_average("Speed")
        highSpeed = self.controller.get_highest("Speed")
        lowEfficiency = self.controller.get_lowest("Efficiency")
        avgEfficiency = self.controller.get_average("Efficiency")
        highEfficiency = self.controller.get_highest("Efficiency")
        bllabel = self.gtk_builder.get_object("bottomLeftLabel")
        bllabel.set_text(
            'Speed (s for 100m): {0:.0f}/{1:.0f}/{2:.0f} \nEfficiency: {3:.2f}/{4:.2f}/{5:.2f}'.format(lowSpeed,
                                                                                                       avgSpeed,
                                                                                                       highSpeed,
                                                                                                       lowEfficiency,
                                                                                                       avgEfficiency,
                                                                                                       highEfficiency))
        distance = self.controller.get_sum('Distance')
        unit = self.controller.get_first('Units')
        bmlabel = self.gtk_builder.get_object('bottomMiddleLabel')
        bmlabel.set_text('Total Distance: {0:,}{1}'.format(distance, unit))
        sessionsAndSets = self.controller.get_consolidated_values()
        brlabel = self.gtk_builder.get_object('bottomRightLabel')
        brlabel.set_text('Total {0} sessions with {1} sets'.format(len(sessionsAndSets), sum(sessionsAndSets)))

    def __init__( self ):

        self.graph_states = []
        self.graph_options = \
        {
            'Distance': { 'label' : 'Distance' , 'stroke': 'k-'},
            'Efficiency': { 'label' : 'Efficiency' , 'stroke': 'b-' },
            'Strokes': { 'label' : 'Strokes' , 'stroke': 'g-' },
            'Speed': { 'label' : 'Speed' , 'stroke': 'r-' }
        }
        gladefile = "mainWindow.glade"
        self.gtk_builder = gtk.Builder()
        self.gtk_builder.add_from_file(gladefile)

        self.controller = Controller()

        self.gtk_builder.connect_signals(self, dict)

        records = self.controller.get_all_records()

        self.plotter = Plotter()

        self.active_records = records

        self.list_store = RowObjectListStore(
               [(str, 'id'),
                (str, 'LogDate'),
                (str, 'LogTime'),
                (int, 'Pool'),
                (str, 'Units'),
                (str, 'TotalDuration'),
                (int, 'Calories'),
                (int, 'TotalLengths'),
                (int, 'TotalDistance'),
                (int, 'Nset'),
                (str, 'Duration'),
                (int, 'Strokes'),
                (int, 'Distance'),
                (int, 'Speed'),
                (int, 'Efficiency'),
                (int, 'StrokeRate')],
                                    records)

        # get columns statically
        types = self.controller.get_column_types()
        self.columns = types.keys()

        treeview = self.gtk_builder.get_object("recordView")
        self.add_columns(treeview)

        # build bottom labels
        self.build_bottom_labels(treeview)

        # build calendar
        self.calendar = self.gtk_builder.get_object("calendar")
        self.build_calendar()
        self.actual_date =  None

        # start gui
        gtk.main()

    def editedCell(self, cell, row, value):
        print "cell {0} with row {1}, new value {2}".format(cell, row, value)
        print self.list_store[int(row)].Units
        swimrecord = self.list_store[int(row)]
        swimrecord.update()

        self.controller.update_record(swimrecord)
        pass

    def on_quitButton_clicked(self, widget, event):
        """calls self.closeApplication"""
        self.closeApplication()

    def on_mainWindow_destroy(self, widget, event):
        """calls self.closeApplication"""
        self.closeApplication()

    def closeApplication(self):
        """common main exit method, a place to do optional cleanup"""
        print "exit"
        gtk.main_quit()


    def update_graph(self):

        self.plotbox = self.gtk_builder.get_object("plotbox")
        # clear old graphs
        if len(self.plotbox.get_children()) > 0:
            [self.plotbox.remove(child) for child in self.plotbox.get_children()]

        fig = self.plotter.get_figure(self.is_single_session_view())

        if fig is None:
            return

        canvas = Canvas(fig)
        canvas.set_size_request(500, 300)
        canvas.queue_draw()
        self.plotbox.add(canvas)
        self.plotbox.show_all()

    def update_plotdata_and_plot(self, label):
        self.toggle_graph_state(label)
        data = self.plotter.convert_session_to_plot_data(self.active_records, label, self.is_single_session_view())
        self.plotter.toggle_plot_data(data, self.graph_options[label])
        self.update_graph()

    def on_speed_clicked(self, *ignore):
        label = 'Speed'
        self.update_plotdata_and_plot(label)

    def on_efficiency_clicked(self, *ignore):
        label = 'Efficiency'
        self.update_plotdata_and_plot(label)

    def on_stroke_clicked(self, *ignore):
        label = 'Strokes'
        self.update_plotdata_and_plot(label)

    def on_distance_clicked(self, *ignore):
        label = 'Distance'
        self.update_plotdata_and_plot(label)


    def on_reset_clicked(self, *ignore):
        self.actual_date =  None
        self.active_records = self.controller.get_all_records()
        self.update_view_data()
        self.update_all_active_graphs()

    def today_clicked(self, *ignore):
        now = datetime.datetime.now()
        self.update_view_with_day(now.year, now.month, now.day)

    def update_view_data(self):
        self.update_recordview()

    def update_recordview(self):
        self.list_store.clear()
        [self.list_store.append(row) for row in self.active_records]

    def build_calendar(self):
        self.update_calendar_date(self.calendar, None)

    def month_changed(self, calendar, type):
        self.update_calendar_date(calendar, type)
    def next_month(self, calendar, type):
        self.update_calendar_date(calendar, type)

    def select_day(self, calendar, type):
        self.update_calendar_date(calendar, type)
        self.actual_date =  calendar.get_date()
        self.update_view_with_day(*calendar.get_date())

    def update_all_active_graphs(self):
        self.plotter.clear()
        for label in self.graph_states:
            data = self.plotter.convert_session_to_plot_data(self.active_records, label, self.is_single_session_view())
            options = self.graph_options[label]
            self.plotter.toggle_plot_data(data, options)
        self.update_graph()

    def update_view_with_day(self, year, month ,day):
        self.active_records = self.controller.get_record_by_date(year, month + 1, day)
        self.update_view_data()

        self.update_all_active_graphs()


    def next_year(self, calendar, type):
        self.update_calendar_date(calendar, type)

    def prev_month(self, calendar, type):
        self.update_calendar_date(calendar, type)

    def prev_year(self, calendar, type):
        self.update_calendar_date(calendar, type)

    def update_calendar_date(self, calendar, type):
        self.calendar.clear_marks()
        year, month, day =  calendar.get_date()
        records_by_date = self.controller.get_record_by_date(year, month + 1)
        for record in records_by_date:
            day = record.day
            self.calendar.mark_day(int(day))
        pass


    def toggle_graph_state(self, label):
        if label in self.graph_states:
            self.graph_states.remove(label)
        else:
            self.graph_states.append(label)

    def is_single_session_view(self):
        """Returns True if the view state shows a single session
         which is true if the user has chosen a specific date.
        """
        return self.actual_date is not None

go = PyPoolmate()
