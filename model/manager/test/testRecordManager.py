from unittest.case import TestCase
import datetime
from db import poolmatedb
from model.manager.recordManager import recordManager

__author__ = 'knm'

import unittest

class recordManagerTest(unittest.TestCase):

    def setUp(self):
        dbname = 'poolmatetest'
        db = poolmatedb.Poolmatedb(dbname=dbname)
        all = db.get_all()
        records = [doc.value for doc in all]
        self.manager = recordManager(records)


    def test_Speed(self):
        h_speed = self.manager.get_highest("Speed")
        l_speed = self.manager.get_lowest("Speed")
        a_speed = self.manager.get_average("Speed")
        self.assertTrue(h_speed >= a_speed)
        self.assertTrue(a_speed >= l_speed)

    def test_getPlotData(self):
        data = self.manager.get_plot_data("Speed")
        self.assertEquals(len(data[0]), 2, "should have 2 dimensions for plotting" )
        self.assertEquals(type(data[0][0]), datetime.datetime, "type x should be of datetime")
        self.assertEquals(type(data[0][1]), float, "type y should be of float")

    def test_createSpeedGraph(self):
        self.manager.create_graph("Speed", None)
