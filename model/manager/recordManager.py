from datetime import datetime

__author__ = 'knm'

class recordManager:
    def __init__(self, allrecords):
        self.allrecords = allrecords

    def get_lowest(self, field):
        return float(min(filter(lambda val: val != 0.0, self.get_field_values_as_float(field))))

    def get_highest(self, field):
        return float(max([val for val in self.get_field_values_as_float(field)]))

    def get_first(self, field):
        return self.get_field_values(field)[0]

    def get_average(self, field):
        allValues = filter(lambda val: val != 0.0,self.get_field_values_as_float(field))
        return sum(allValues) / float(len(allValues))

    def get_sum(self, field):
        return sum([float(val) for val in self.get_field_values(field)])
    
    def get_field_values(self,field):
        return [record[field] for record in self.allrecords]

    def get_field_values_as_float(self,field):
        return [float(record[field]) for record in self.allrecords]

    def get_all_records(self):
        return self.allrecords

    def create_graph(self, label, options=None):
        x_values = self.get_field_values_as_float(label)
#        y_values = self.getF
        pass

    def get_plot_data(self, field, x_field = "LogDateTime"):
        y_data = self.get_field_values_as_float(field)
        x_data = [datetime.strptime(date, '%Y-%m-%dT%H:%M:%S') for date in self.get_field_values(x_field)]
        return zip(x_data, y_data)

