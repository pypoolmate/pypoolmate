import md5
from datetime import datetime

class ImportRecord:
    def __init__(self, row):
        """Does create a import record from given date. It's id will be a md5 calculation of its concatenated properties.
        """
        self.User,self.LogDate,self.LogTime,self.LogType,self.Pool,self.Units,self.TotalDuration,self.Calories,self.TotalLengths,self.TotalDistance,self.Nset,self.Duration,self.Strokes,self.Distance,self.Speed,self.Efficiency,self.StrokeRate,self.HRmin,self.HRmax,self.HRavg,self.HRbegin,self.HRend,self.Version = row
        m = md5.new()
        m.update(self.User + self.LogDate + self.LogTime + self.LogType + self.Pool + self.Units + self.TotalDuration + self.Calories + self.TotalLengths + self.TotalDistance + self.Nset + self.Duration + self.Strokes + self.Distance + self.Speed + self.Efficiency + self.StrokeRate + self.HRmin + self.HRmax + self.HRavg + self.HRbegin + self.HRend + self.Version)
        self.id = m.hexdigest()

    def id(self):
        return self.id
    
    def jsonize(self):
        """Returns a json representation of the swim record.

        It will containing float and string values.

        Invalid Errors do raise an ValueError. An Invalid record is one where distance, stroke rate or number of strokes is 0
        """
        if self.Distance == '0' or \
           self.StrokeRate == '0' or \
           self.Strokes == '0':
             raise ValueError

        return {
                'User': self.User,
                'LogDate': self.LogDate,
                'LogTime': self.LogTime,
                'LogType': self.LogType,
                'Pool': float(self.Pool),
                'Units': self.Units,
                'TotalDuration': self.TotalDuration,
                'Calories': float(self.Calories),
                'TotalLengths': float(self.TotalLengths),
                'TotalDistance': float(self.TotalDistance),
                'Nset': float(self.Nset),
                'Duration': self.Duration,
                'Strokes': float(self.Strokes),
                'Distance': float(self.Distance),
                'Speed': float(self.Speed),
                'Efficiency': float(self.Efficiency),
                'StrokeRate': float(self.StrokeRate),
                'HRmin': self.HRmin,
                'HRmax': self.HRmax,
                'HRavg': self.HRavg,
                'HRbegin': self.HRbegin,
                'HRend': self.HRend,
                'Version': self.Version,
                # customized entries
                'type': 'swimrecord',
                'LogDateTime':datetime.strptime(self.LogDate + '-' + self.LogTime, '%d/%m/%Y-%H:%M:%S').isoformat(),
                'day':int(self.LogDate[:2]),
                'month':int(self.LogDate[3:5]),
                'year':int(self.LogDate[-4:]),
                'parentchildtype':'child',

        }

    def get(self):
        return self.User,self.LogDate,self.LogTime,self.LogType,self.Pool,self.Units,\
               self.TotalDuration,self.Calories,self.TotalLengths,self.TotalDistance,\
               self.Nset,self.Duration,self.Strokes,self.Distance,self.Speed,\
               self.Efficiency,self.StrokeRate,self.HRmin,self.HRmax,self.HRavg,\
               self.HRbegin,self.HRend,self.Version
