from collections import OrderedDict
import gobject

__author__ = 'knm'

class swimrecord:
    """A data holder class to define columns and their types to be displayed in the view """

    def __init__(self, recordFromDB=None):
        self.record = recordFromDB
        if recordFromDB:
            self.id = recordFromDB['_id']
            self.day = recordFromDB['day']
            self.month = recordFromDB['month']
            self.year = recordFromDB['year']
            self.LogDate = recordFromDB['LogDate']
            self.LogTime = recordFromDB['LogTime']
            self.Pool = int(recordFromDB['Pool'])
            self.Units = recordFromDB['Units']
            self.TotalDuration = recordFromDB['TotalDuration']
            self.Calories = float(recordFromDB['Calories'])
            self.TotalLengths = float(recordFromDB['TotalLengths'])
            self.TotalDistance = float(recordFromDB['TotalDistance'])
            self.Nset = float(recordFromDB['Nset'])
            self.Duration = recordFromDB['Duration']
            self.Strokes = float(recordFromDB['Strokes'])
            self.Distance = float(recordFromDB['Distance'])
            self.Speed = float(recordFromDB['Speed'])
            self.Efficiency = float(recordFromDB['Efficiency'])
            self.StrokeRate = float(recordFromDB['StrokeRate'])
        self.columnTypes = OrderedDict([('_id',gobject.TYPE_STRING),
                             ('LogDate',gobject.TYPE_STRING),
                             ('LogTime',gobject.TYPE_STRING),
                             ('Pool',gobject.TYPE_STRING),
                             ('Units',gobject.TYPE_STRING),
                             ('TotalDuration',gobject.TYPE_STRING),
                             ('Calories',gobject.TYPE_STRING),
                             ('TotalLengths',gobject.TYPE_STRING),
                             ('TotalDistance',gobject.TYPE_STRING),
                             ('Nset',gobject.TYPE_STRING),
                             ('Duration',gobject.TYPE_STRING),
                             ('Strokes',gobject.TYPE_STRING),
                             ('Distance',gobject.TYPE_STRING),
                             ('Speed',gobject.TYPE_STRING),
                             ('Efficiency',gobject.TYPE_STRING),
                             ('StrokeRate',gobject.TYPE_STRING)])

    def getRecordValues(self, titles):
        return [self.record[title] for title in titles]

    def get_column_types(self):
        return self.columnTypes

    def update(self):
        self.record['Pool'] = int(self.Pool)
        self.record['Units'] = self.Units
        self.record['TotalDuration'] = self.TotalDuration
        self.record['Calories'] = int(self.Calories)
        self.record['TotalLengths'] = int(self.TotalLengths)
        self.record['TotalDistance'] = int(self.TotalDistance)
        self.record['Nset'] = int(self.Nset)
        self.record['Duration'] = self.Duration
        self.record['Strokes'] = int(self.Strokes)
        self.record['Distance'] = int(self.Distance)
        self.record['Speed'] = int(self.Speed)
        self.record['Efficiency'] = int(self.Efficiency)
        self.record['StrokeRate'] = int(self.StrokeRate)

