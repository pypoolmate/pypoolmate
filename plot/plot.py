from matplotlib.dates import num2date
from datetime import datetime
import matplotlib.ticker as ticker
from hashlib import sha1
import matplotlib.pyplot as plt

__author__ = 'knm'


class Plotter:

    def __init__(self):
        self.data = []
        pass

    def clear(self):
        self.data = []

    def toggle_plot_data(self, data, options=None):
        """Adds data and options to current data
        If already present it will be removed from current data"""

        if data is None:
            return

        new_plotunit = PlotUnit(data=data, options=options)

        alreadPresent = False;
        for plotunit in self.data:
            if plotunit.key == new_plotunit.key:
                self.data.remove(plotunit)
                alreadPresent = True

        if not alreadPresent:
            self.data.append(new_plotunit)

    def make_patch_spines_invisible(self, ax):
        ax.set_frame_on(True)
        ax.patch.set_visible(False)
        for sp in ax.spines.itervalues():
            sp.set_visible(False)

    def get_figure(self, is_single_session_view=False):
        """creating the plot for current data"""

        if not len(self.data):
            return None

        fig = plt.figure()
        if len(self.data) == 3:
            fig.subplots_adjust(right=0.85)
        if len(self.data) == 4:
            fig.subplots_adjust(right=0.8)
        ax = fig.add_subplot(111)
        lines = []
        tkw = dict(size=4, width=1.0)
        for i, plotunit in enumerate(self.data):
            data = plotunit.data
            options = plotunit.options
            data.sort()
            dates = [q[0] for q in data]
            vals = [q[1] for q in data]
            if not i:
                p1, = ax.plot(dates, vals, options['stroke'], label=options['label'])
                ax.tick_params(axis='y', colors=p1.get_color(), **tkw)
                ax.set_ylabel(options['label'])
                lines.append(p1)
                [tick.label.set_color(p1.get_color()) for tick in ax.yaxis.get_major_ticks()]
                ax.yaxis.label.set_color(p1.get_color())
            elif i > 1:
                clonedAxes = ax.twinx()
                spine = None
                if i == 2:
                    spine = clonedAxes.spines['right']
                    spine.set_position(('axes', 1.1))
                if i == 3:
                    spine = clonedAxes.spines['right']
                    spine.set_position(('axes', 1.2))
                self.make_patch_spines_invisible(clonedAxes)
                spine.set_visible(True)
                p3, = clonedAxes.plot(dates, vals, options['stroke'], label=options['label'])

                clonedAxes.set_ylabel(options['label'])
                [tick.label.set_color(p1.get_color()) for tick in clonedAxes.yaxis.get_major_ticks()]
                clonedAxes.yaxis.label.set_color(p3.get_color())
                clonedAxes.tick_params(axis='y', colors=p3.get_color(), **tkw)
                lines.append(p3,)
            else:
                clonedAxes = ax.twinx()

                p2, = clonedAxes.plot(dates, vals, options['stroke'], label=options['label'])
                clonedAxes.tick_params(axis='y', colors=p2.get_color(), **tkw)
                clonedAxes.set_ylabel(options['label'])
                [tick.label.set_color(p2.get_color()) for tick in clonedAxes.yaxis.get_major_ticks()]
                clonedAxes.yaxis.label.set_color(p2.get_color())
                lines.append(p2,)

        ax.tick_params(axis='x', **tkw)

        if is_single_session_view:
            ax.xaxis.set_major_locator(ticker.FixedLocator([i for i in range(1,1+len(self.data[0].data))]))
            ax.xaxis.set_major_formatter(ticker.FuncFormatter(self.format_set))

        ax.legend(lines, [l.get_label() for l in lines],0)

#        if not is_single_session_view:
#            ax.xaxis.set_major_formatter(ticker.FuncFormatter(self.format_date))

        return fig

    def format_date(self, x, pos=None):
        date = num2date(x)
        if date.month == 1:
            return date.strftime('%d %b %Y')
        else:
            return date.strftime('%d %b')

    def format_set(self, x, pos=None):
        return 'Set ' + str(x)

    def convert_session_to_plot_data(self, sessions, property, is_single_session_view=False):
        if is_single_session_view:
            data = [(session.record['Nset'], session.record[property]) for session in sessions]
        else:
            data = [(datetime.strptime(session.record['LogDateTime'], '%Y-%m-%dT%H:%M:%S'), session.record[property]) for session in sessions]

        return data

class PlotUnit:

    def __init__(self, data, options):
        self.data = data
        self.options = options
        self.key = sha1(str(data) + str(options)).hexdigest()

    def get_key(self):
        return self.key
