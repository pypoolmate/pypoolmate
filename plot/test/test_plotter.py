from unittest import TestCase
from controller.controller import Controller
from db import poolmatedb
from plot.plot import Plotter

__author__ = 'knm'

class TestPlotter(TestCase):

    def setUp(self):
        self.dbname = 'poolmatetest'
        self.db = poolmatedb.Poolmatedb(dbname=self.dbname)

    def test_convert_session_to_plot_data(self):
        date = '01/07/2011'
        controller = Controller()
        results = controller.get_record_by_date(2011, 7, 1)


        self.assertEquals(4, len(results))
        plotter = Plotter()
        data = plotter.convert_session_to_plot_data(results, 'Speed')
        self.assertEquals(4, len(data))

